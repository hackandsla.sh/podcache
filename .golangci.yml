linters-settings:
  dupl:
    threshold: 100
  funlen:
    lines: 100
    statements: 50
  goconst:
    min-len: 2
    min-occurrences: 2
  gocritic:
    enabled-tags:
      - diagnostic
      - experimental
      - opinionated
      - performance
      - style
    disabled-checks:
      - ifElseChain
      - wrapperFunc
      - hugeParam
      - rangeValCopy
  gocyclo:
    min-complexity: 15
  cyclop:
    max-complexity: 15
  godox:
    # report any comments starting with keywords, this is useful for TODO or FIXME comments that
    # might be left in the code accidentally and should be resolved before merging
    keywords: # default keywords are TODO, BUG, and FIXME, these can be overwritten by this setting
      #- TODO # Allowed for marking future code enhancements
      - BUG
      - FIXME
      #- NOTE # Allowed for marking "gotchas"
      - OPTIMIZE # marks code that should be optimized before merging
      - HACK # marks hack-arounds that should be removed before merging
  goimports:
    local-prefixes: gitlab.com/hackandsla.sh/podcache
  gomnd:
    settings:
      mnd:
        # don't include the "operation" and "assign"
        checks:
          - argument
          - case
          - condition
          - return
  govet:
    check-shadowing: true
  lll:
    line-length: 140
  maligned:
    suggest-new: true
  misspell:
    locale: US
  tagliatelle:
    # check the struck tag name case
    case:
      # use the struct field name to check the name of the struct tag
      use-field-name: true
      rules:
        json: camel
        yaml: camel
  linters-settings:
  wrapcheck:
    ignorePackageGlobs:
      - encoding/*
      - github.com/pkg/*
      - google.golang.org/grpc/*
      - golang.org/x/sync/errgroup
      - github.com/go-ozzo/ozzo-validation/v4
  wsl:
    allow-cuddle-declarations: true

linters:
  # please, do not use `enable-all`: it's deprecated and will be removed soon.
  # inverted configuration with `enable-all` and `disable` is not scalable during updates of golangci-lint
  disable-all: true
  enable:
    - asciicheck
    - bodyclose
    # - contextcheck # v1.43.0
    - cyclop
    - deadcode
    - depguard
    - dogsled
    - dupl
    - durationcheck
    - errcheck
    - errname
    - errorlint
    - exhaustive
    # - exhaustivestruct
    - exportloopref
    # - forbidigo
    - forcetypeassert
    - funlen
    # - gci
    - gochecknoglobals
    - gochecknoinits
    - gocognit
    - goconst
    - gocritic
    - gocyclo
    - godot
    - godox
    - goerr113
    - gofmt
    - gofumpt
    # - goheader
    - goimports
    # - golint # deprecated
    - gomnd
    - gomodguard
    - goprintffuncname
    - gosec
    - gosimple
    - govet
    - ineffassign
    - ifshort
    - importas
    # - interfacer # deprecated
    # - ireturn # v1.43.0
    - lll
    - makezero
    # - maligned
    - misspell
    - nakedret
    - nestif
    - nilerr
    # - nilnil # v1.43.0
    - nlreturn
    - noctx
    - nolintlint
    - paralleltest
    - prealloc
    - predeclared
    # - promlinter
    - revive
    - rowserrcheck
    # - scopelint # deprecated
    - sqlclosecheck
    - staticcheck
    - structcheck
    - stylecheck
    - tagliatelle
    # - tenv # v1.43.0
    - testpackage
    - thelper
    - tparallel
    - typecheck
    - unconvert
    - unparam
    - unused
    - varcheck
    - wastedassign
    - whitespace
    - wrapcheck
    - wsl

issues:
  # Excluding configuration per-path, per-linter, per-text and per-source
  exclude-rules:
    - path: _test\.go
      linters:
        - gomnd
        - bodyclose
        - scopelint
        - funlen
        - goerr113
    - path: _test\.go
      source: "assert := "
      linters:
        - gocritic
    - path: handlers\.go
      linters:
        - dupl
    - path: errors\.go
      linters:
        - golint
    - source: "embed.FS"
      linters:
        - gochecknoglobals
    - path: cmd
      linters:
        - wrapcheck