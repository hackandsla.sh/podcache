APP = ./podcache
SHELL = bash
export

GO_LINT_VERSION = v1.50.1
JUNIT_VERSION = v0.9.1
LSIF_GO_VERSION = latest
SQLC_VERSION = v1.16.0

dev:
	PODCACHE_DATADIR="./devdata" go run .

remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|main)" | xargs git branch -d

migrate:
	PODCACHE_DATADIR="./devdata" go run . migrate

migrate-down:
	PODCACHE_DATADIR="./devdata" go run . migrate --downgrade

lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@${GO_LINT_VERSION} run ./... -v --timeout 5m

tidy:
	./build/tidy.sh

lsif:
	go run github.com/sourcegraph/lsif-go/cmd/lsif-go@${LSIF_GO_VERSION}

junit:
	go test -covermode=count ./... -v 2>&1 | go run github.com/jstemmer/go-junit-report@${JUNIT_VERSION} -set-exit-code > report.xml

.PHONY: build proto

sqlc:
	go run github.com/kyleconroy/sqlc/cmd/sqlc@${SQLC_VERSION} generate