# Podcache

## Overview

Podcache is a tool to automatically archive podcasts

## Features

- Automatically download new episodes of podcasts as they come out
- Save copies of episodes for offline listening

## Quickstart

- Download the appropriate binary from the [releases](https://gitlab.com/hackandsla.sh/podcache/-/releases) page.
- In the directory you want to archive to, create a file called `podcache.yaml`. It should look something like this:

    ```yaml
    podcasts:
      - url: https://example.com/rss
      - url: https://other.example.com/rss
    ```

- Run the `podcache` tool with the path to the directory:

    ```bash
    PODCACHE_DATADIR=/path/to/archive podcache start
    ```

Episodes will start getting downloaded to that directory with the following naming scheme:

```txt
<podcast-name>/<date> <episode-name>.mp3
```

## Docker Quickstart

To run this project with Docker, do the following:

- Create the `podcache.yaml` file as described above
- Run the `docker` command mounting that directory to the `/data` directory.

    ```bash
    docker run -v /path/to/archive:/data gitlab.com/hackandsla.sh/podcache:latest
    ```

## License

This project is [MIT](./LICENSE) licensed
