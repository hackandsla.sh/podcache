//go:build go1.12

package main

import (
	"fmt"
	"runtime/debug"
)

// info for people who "go get" this package.
//
//nolint:gochecknoinits // This is used to dynamically insert version and commit
func init() {
	if info, available := debug.ReadBuildInfo(); available {
		if date == "" {
			version = info.Main.Version
			commit = fmt.Sprintf("(unknown, mod sum: %q)", info.Main.Sum)
			date = "(unknown)"
		}
	}
}
