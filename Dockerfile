FROM gcr.io/distroless/static-debian11:nonroot as final
LABEL author="Trevor Taubitz"

# Run as unprivileged
USER 1000:1000

ENV PODCACHE_DATADIR "/data"

ARG BIN
COPY $BIN "/main"
ENTRYPOINT ["/main", "start"]
