-- name: AddEpisode :exec
insert into episodes (podcast_id, name, url, guid, status, published_at)
values (?, ?, ?, ?, ?, ?);

-- name: GetNextDownloadItem :one
update episodes
set status = 'downloading'
where episode_id = (
  select min(episode_id)
  from episodes
  where status = 'new')
returning *;

-- name: GetEpisodeIDsByStatus :many
select episode_id
from episodes
where status = ?;

-- name: SetEpisodeStatus :exec
update episodes
set status = ?
where episode_id = ?;

-- name: SetEpisodeArchivePath :exec
update episodes
set archive_path = ?
where episode_id = ?;

-- name: GetEpisodeByGUID :one
select * from episodes
where guid == ?;