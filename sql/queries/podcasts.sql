-- name: AddPodcast :one
insert into podcasts (name, rss_url)
values (?, ?)
on conflict(rss_url) do update
  set name = excluded.name
returning *;

-- name: GetPodcast :one
select * from podcasts
where podcast_id = ?;

-- name: GetPodcasts :many
select * from podcasts;

-- name: UpdatePodcastRefreshedAt :exec
update podcasts
set last_refreshed_at = current_timestamp
where podcast_id = ?;