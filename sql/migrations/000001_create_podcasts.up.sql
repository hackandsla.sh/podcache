create table podcasts (
    podcast_id integer primary key autoincrement,
    name text not null,
    rss_url text not null,
    created_at timestamp default current_timestamp not null,
    updated_at timestamp default current_timestamp not null,
    last_refreshed_at timestamp default current_timestamp not null
);

create unique index rss_url_idx on podcasts(rss_url);

create table episodes (
    episode_id integer primary key autoincrement,
    podcast_id integer not null,
    name text not null,
    url text not null,
    guid text not null,
    status text not null,
    archive_path text,
    published_at timestamp not null,
    foreign key(podcast_id) references podcasts(podcast_id)
);

create unique index guid_idx on episodes(guid);