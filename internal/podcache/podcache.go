package podcache

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"time"

	"github.com/cavaliergopher/grab/v3"
	"github.com/flytam/filenamify"
	"github.com/golang-migrate/migrate/v4"
	"github.com/mmcdole/gofeed"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/yaml.v3"
	_ "modernc.org/sqlite" // Driver for sqlite DB

	"gitlab.com/hackandsla.sh/podcache/internal/sqlitedb"
)

const (
	dbFileName        = "podcache.db"
	archiveFolderName = "archive"
	configFileName    = "podcache.yaml"
)

type Config struct {
	DataDir              string
	ArchiveWorkers       int
	ArchivePollFrequency time.Duration
	FeedUpdateFrequency  time.Duration
	LogLevel             string
}

const (
	downloadStatusNew        = "new"
	downloadStatusInProgress = "downloading"
	downloadStatusDownloaded = "downloaded"
)

func RunArchiver(cfg Config, migrations fs.FS) error {
	db, err := GetDBConn(cfg.DataDir)
	if err != nil {
		return fmt.Errorf("couldn't open database: %w", err)
	}

	m, err := NewMigrations(migrations, db)
	if err != nil {
		return fmt.Errorf("couldn't initialize migrations: %w", err)
	}

	// Automatically apply migrations for simplicity
	if err1 := m.Up(); err1 != nil && !errors.Is(err1, migrate.ErrNoChange) {
		return fmt.Errorf("error while applying migrations: %w", err1)
	}

	log, err := newLogger(LoggerConfig{
		Level: cfg.LogLevel,
	})
	if err != nil {
		return fmt.Errorf("couldn't build logger: %w", err)
	}

	srv := NewServer(cfg, db, log)

	if err1 := srv.resetInProgressDownloads(context.Background()); err1 != nil {
		return fmt.Errorf("couldn't reset in-progress downloads: %w", err1)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Kill, os.Interrupt)
	defer cancel()

	configFile := filepath.Join(cfg.DataDir, configFileName)

	fileConfig, err := readConfigFile(configFile)
	if err != nil {
		return fmt.Errorf("error reading config file '%s': %w", configFile, err)
	}

	for _, podcast := range fileConfig.Podcasts {
		if err := srv.AddPodcast(ctx, AddPodcastRequest{RSSURL: podcast.URL}); err != nil {
			return fmt.Errorf("error adding podcast with URL '%s': %w", podcast.URL, err)
		}
	}

	startWorkers(ctx, srv)

	go runCron(ctx, cfg.ArchivePollFrequency, func() {
		if err := srv.ArchivePodcastEpisode(ctx); err != nil {
			log.Error("couldn't archive podcast episode", zap.Error(err))
		}
	})

	go runCron(ctx, cfg.FeedUpdateFrequency, func() {
		if err := srv.RefreshRSSFeeds(ctx); err != nil {
			log.Error("error refreshing RSS feeds", zap.Error(err))
		}
	})

	log.Info("server started")

	<-ctx.Done()

	log.Info("stopping server")

	return nil
}

type Server struct {
	db  *sql.DB
	cfg Config
	log *zap.Logger

	episodeDownloadCh chan sqlitedb.Episode
}

func NewServer(cfg Config, db *sql.DB, log *zap.Logger) *Server {
	return &Server{
		db:                db,
		cfg:               cfg,
		log:               log,
		episodeDownloadCh: make(chan sqlitedb.Episode),
	}
}

type AddPodcastRequest struct {
	RSSURL string
}

func (s *Server) AddPodcast(ctx context.Context, req AddPodcastRequest) error {
	fp := gofeed.NewParser()

	feed, err := fp.ParseURL(req.RSSURL)
	if err != nil {
		return fmt.Errorf("couldn't parse feed URL: %w", err)
	}

	queries := sqlitedb.New(s.db)

	podcast, err := queries.AddPodcast(ctx, sqlitedb.AddPodcastParams{
		Name:   feed.Title,
		RssUrl: req.RSSURL,
	})
	if err != nil {
		return fmt.Errorf("couldn't add podcast to database: %w", err)
	}

	if err := s.refreshRSSFeed(ctx, podcast); err != nil {
		return fmt.Errorf("couldn't update RSS feed: %w", err)
	}

	return nil
}

func (s *Server) RefreshRSSFeeds(ctx context.Context) error {
	queries := sqlitedb.New(s.db)

	podcasts, err := queries.GetPodcasts(ctx)
	if err != nil {
		return fmt.Errorf("couldn't retrieve podcasts: %w", err)
	}

	for _, podcast := range podcasts {
		if err := s.refreshRSSFeed(ctx, podcast); err != nil {
			s.log.Error("couldn't refresh RSS feed",
				zap.Error(err),
				zap.String("podcast", podcast.Name),
			)
		}
	}

	return nil
}

func (s *Server) refreshRSSFeed(ctx context.Context, podcast sqlitedb.Podcast) error {
	log := s.log.With(zap.String("podcast", podcast.Name))
	log.Info("refreshing RSS feed")

	fp := gofeed.NewParser()

	feed, err := fp.ParseURL(podcast.RssUrl)
	if err != nil {
		return fmt.Errorf("couldn't parse feed URL: %w", err)
	}

	queries := sqlitedb.New(s.db)

	var newEpisodeCount int

	for _, item := range feed.Items {
		_, err := queries.GetEpisodeByGUID(ctx, item.GUID)
		if !errors.Is(err, sql.ErrNoRows) {
			continue
		}

		newEpisodeCount++

		if err := queries.AddEpisode(ctx, sqlitedb.AddEpisodeParams{
			PodcastID:   podcast.PodcastID,
			Name:        item.Title,
			Url:         item.Enclosures[0].URL,
			Guid:        item.GUID,
			Status:      downloadStatusNew,
			PublishedAt: *item.PublishedParsed,
		}); err != nil {
			return fmt.Errorf("error adding episode: %w", err)
		}
	}

	if newEpisodeCount > 0 {
		log.Info("found new episodes", zap.Int("count", newEpisodeCount))
	}

	if err := queries.UpdatePodcastRefreshedAt(ctx, podcast.PodcastID); err != nil {
		return fmt.Errorf("couldn't update podcast '%s': %w", podcast.Name, err)
	}

	return nil
}

func (s *Server) ArchivePodcastEpisode(ctx context.Context) error {
	queries := sqlitedb.New(s.db)

	episodeToDownload, err := queries.GetNextDownloadItem(ctx)

	switch {
	case errors.Is(err, sql.ErrNoRows):
		return nil
	case err != nil:
		return fmt.Errorf("couldn't get episodes: %w", err)
	}

	s.episodeDownloadCh <- episodeToDownload

	return nil
}

type EpisodeDownloadWorker struct {
	db                *sql.DB
	episodeDownloadCh <-chan sqlitedb.Episode
	archivePath       string
	fileDownloader    *grab.Client
	log               *zap.Logger
}

func (w *EpisodeDownloadWorker) Run(ctx context.Context) {
	for episode := range w.episodeDownloadCh {
		if err := w.downloadEpisode(ctx, episode); err != nil {
			w.log.Error("error downloading episode",
				zap.Error(err),
				zap.String("episode", episode.Name),
			)
		}
	}
}

func (w *EpisodeDownloadWorker) downloadEpisode(ctx context.Context, episode sqlitedb.Episode) error {
	log := w.log.With(zap.String("episode", episode.Name))
	filename := fmt.Sprintf("%s %s.mp3", episode.PublishedAt.Format("2006-01-02"), episode.Name)

	filename = sanitizeFilename(filename)

	queries := sqlitedb.New(w.db)

	podcast, err := queries.GetPodcast(ctx, episode.PodcastID)
	if err != nil {
		return fmt.Errorf("couldn't lookup podcast: %w", err)
	}

	path := filepath.Join(w.archivePath, podcast.Name, filename)

	log = log.With(
		zap.String("filepath", path),
		zap.String("podcast", podcast.Name),
	)

	if err1 := queries.SetEpisodeArchivePath(ctx, sqlitedb.SetEpisodeArchivePathParams{
		EpisodeID:   episode.EpisodeID,
		ArchivePath: sql.NullString{String: path, Valid: true},
	}); err1 != nil {
		return fmt.Errorf("couldn't set episode archive location: %w", err1)
	}

	req, err := grab.NewRequest(path, episode.Url)
	if err != nil {
		return fmt.Errorf("couldn't create download request: %w", err)
	}

	req = req.WithContext(ctx)

	res := w.fileDownloader.Do(req)

	if res.DidResume {
		log.Info("resuming download")
	} else {
		log.Info("starting download")
	}

	if err := res.Err(); err != nil && !res.IsComplete() {
		return fmt.Errorf("error while downloading file: %w", err)
	}

	if err := queries.SetEpisodeStatus(ctx, sqlitedb.SetEpisodeStatusParams{
		EpisodeID: episode.EpisodeID,
		Status:    downloadStatusDownloaded,
	}); err != nil {
		return fmt.Errorf("couldn't set episode status: %w", err)
	}

	log.Info("finished downloading")

	return nil
}

// runCron runs a given function at the given interval. The job is canceled when
// the ctx is canceled.
func runCron(ctx context.Context, freq time.Duration, fn func()) {
	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(freq):
			fn()
		}
	}
}

func sanitizeFilename(filename string) string {
	filename = strings.ReplaceAll(filename, ":", " -")
	filename, _ = filenamify.FilenamifyV2(filename)

	return filename
}

func GetDBConn(dataDir string) (*sql.DB, error) {
	dbPath := filepath.Join(dataDir, dbFileName)

	db, err := sql.Open("sqlite", dbPath)
	if err != nil {
		return nil, fmt.Errorf("couldn't open sqlite DB at '%s': %w", dbPath, err)
	}

	return db, nil
}

// Resets the status for all in-progress downloads back to "new".
func (s *Server) resetInProgressDownloads(ctx context.Context) error {
	queries := sqlitedb.New(s.db)

	episodeIDs, err := queries.GetEpisodeIDsByStatus(ctx, downloadStatusInProgress)
	if err != nil {
		return fmt.Errorf("couldn't get in-progress downloads: %w", err)
	}

	for _, episodeID := range episodeIDs {
		err := queries.SetEpisodeStatus(ctx, sqlitedb.SetEpisodeStatusParams{
			Status:    downloadStatusNew,
			EpisodeID: episodeID,
		})
		if err != nil {
			return fmt.Errorf("couldn't set episode status: %w", err)
		}
	}

	return nil
}

type LoggerConfig struct {
	Level string
}

func newLogger(cfg LoggerConfig) (*zap.Logger, error) {
	zapCfg := zap.NewProductionConfig()
	zapCfg.Encoding = "console"
	zapCfg.DisableCaller = true
	zapCfg.DisableStacktrace = true
	zapCfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	if cfg.Level != "" {
		level, err := zap.ParseAtomicLevel(cfg.Level)
		if err != nil {
			return nil, fmt.Errorf("couldn't parse log level '%s': %w", cfg.Level, err)
		}

		zapCfg.Level = level
	}

	log, err := zapCfg.Build()
	if err != nil {
		return nil, fmt.Errorf("couldn't build logger: %w", err)
	}

	return log, nil
}

func startWorkers(ctx context.Context, srv *Server) {
	var workers []EpisodeDownloadWorker
	for i := 0; i < srv.cfg.ArchiveWorkers; i++ {
		workers = append(workers, EpisodeDownloadWorker{
			db:                srv.db,
			episodeDownloadCh: srv.episodeDownloadCh,
			archivePath:       filepath.Join(srv.cfg.DataDir, archiveFolderName),
			fileDownloader:    grab.NewClient(),
			log:               srv.log,
		})
	}

	for _, worker := range workers {
		go worker.Run(ctx)
	}
}

type ConfigFile struct {
	Podcasts []struct {
		URL string `yaml:"url,omitempty"`
	} `yaml:"podcasts,omitempty"`
}

func readConfigFile(path string) (*ConfigFile, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("unable to read file: %w", err)
	}

	var cfg ConfigFile
	if err := yaml.Unmarshal(b, &cfg); err != nil {
		return nil, fmt.Errorf("unable to parse file: %w", err)
	}

	return &cfg, nil
}
