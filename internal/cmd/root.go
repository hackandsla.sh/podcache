package cmd

import (
	"io/fs"

	"github.com/urfave/cli/v2"
)

type GlobalFlags struct {
	DataDir string
}

func Root(info *BuildInfo, migrations fs.FS) *cli.App {
	var globalFlags GlobalFlags

	return &cli.App{
		Name:    info.App,
		Usage:   "A podcast archiver service",
		Version: info.Version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "datadir",
				Aliases:     []string{"d"},
				Usage:       "Use `PATH` as root data directory",
				Value:       ".",
				EnvVars:     []string{"PODCACHE_DATADIR"},
				Destination: &globalFlags.DataDir,
			},
		},
		Commands: []*cli.Command{
			Version(info),
			Migrate(migrations, &globalFlags),
			Start(migrations, &globalFlags),
		},
	}
}
