package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/urfave/cli/v2"
)

type BuildInfo struct {
	App     string `json:"app"`
	Version string `json:"version"`
	Commit  string `json:"commit"`
	Date    string `json:"date"`
	BuiltBy string `json:"builtBy"`
}

func Version(info *BuildInfo) *cli.Command {
	return &cli.Command{
		Name:  "version",
		Usage: "Print detailed version info about the app",
		Action: func(_ *cli.Context) error {
			j, err := json.MarshalIndent(&info, "", "  ")
			if err != nil {
				return fmt.Errorf("error while displaying version: %w", err)
			}

			fmt.Println(string(j))

			return nil
		},
	}
}
