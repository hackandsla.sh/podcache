package cmd

import (
	"errors"
	"fmt"
	"io/fs"

	"github.com/golang-migrate/migrate/v4"
	"github.com/urfave/cli/v2"
	_ "modernc.org/sqlite" // Driver for sqlite DB

	"gitlab.com/hackandsla.sh/podcache/internal/podcache"
)

func Migrate(migrations fs.FS, gFlags *GlobalFlags) *cli.Command {
	type MigrateFlags struct {
		Downgrade bool
	}

	var flags MigrateFlags

	return &cli.Command{
		Name:  "migrate",
		Usage: "Manually run a database migration",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "downgrade",
				Usage:       "If specified, runs a downgrade on the database",
				Hidden:      true,
				Destination: &flags.Downgrade,
			},
		},
		Action: func(_ *cli.Context) error {
			db, err := podcache.GetDBConn(gFlags.DataDir)
			if err != nil {
				return fmt.Errorf("couldn't initialize database connection: %w", err)
			}

			m, err := podcache.NewMigrations(migrations, db)
			if err != nil {
				return fmt.Errorf("couldn't initialize migrations: %w", err)
			}

			if flags.Downgrade {
				err = m.Down()
			} else {
				err = m.Up()
			}

			switch {
			case errors.Is(err, migrate.ErrNoChange):
				fmt.Println("Already at latest migration version")
			case errors.Is(err, migrate.ErrNilVersion):
			// Ignore this error
			case err != nil:
				return fmt.Errorf("error applying migrations: %w", err)
			}

			currentVersion, _, _ := m.Version()

			fmt.Printf("Current Migration Version: %v\n", currentVersion)

			return nil
		},
	}
}
