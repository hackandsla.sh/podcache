package cmd

import (
	"io/fs"
	"time"

	"github.com/urfave/cli/v2"

	"gitlab.com/hackandsla.sh/podcache/internal/podcache"
)

func Start(migrations fs.FS, gFlags *GlobalFlags) *cli.Command {
	type StartFlags struct {
		WorkerCount     int
		UpdateFrequency time.Duration
		LogLevel        string
	}

	var flags StartFlags

	return &cli.Command{
		Name:  "start",
		Usage: "Run the podcache service",
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:        "workers",
				Usage:       "The `number` of concurrent download workers",
				Value:       5,
				Destination: &flags.WorkerCount,
				EnvVars:     []string{"PODCACHE_WORKER_COUNT"},
			},
			&cli.DurationFlag{
				Name:        "update-frequency",
				Usage:       "The frequency to poll for undownloaded episodes",
				Value:       time.Hour,
				Destination: &flags.UpdateFrequency,
				EnvVars:     []string{"PODCACHE_UPDATE_FREQUENCY"},
			},
			&cli.StringFlag{
				Name:        "log-level",
				Usage:       "The `level` to set log verbosity to",
				Value:       "info",
				Destination: &flags.LogLevel,
				EnvVars:     []string{"PODCACHE_LOG_LEVEL"},
			},
		},
		Action: func(_ *cli.Context) error {
			cfg := podcache.Config{
				DataDir:              gFlags.DataDir,
				ArchiveWorkers:       flags.WorkerCount,
				FeedUpdateFrequency:  flags.UpdateFrequency,
				LogLevel:             flags.LogLevel,
				ArchivePollFrequency: 1 * time.Second,
			}

			return podcache.RunArchiver(cfg, migrations)
		},
	}
}
