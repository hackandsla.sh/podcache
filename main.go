package main

import (
	"embed"
	"io/fs"
	"log"
	"os"

	"gitlab.com/hackandsla.sh/podcache/internal/cmd"
)

//nolint:gochecknoglobals // These variables may be dynamically inserted at build-time
var (
	version = ""
	commit  = ""
	date    = ""
	builtBy = ""
	appName = "podcache"
)

//go:embed sql/migrations/*
var migrations embed.FS

func main() {
	migrationsSub, err := fs.Sub(migrations, "sql/migrations")
	if err != nil {
		log.Fatalf("couldn't find database migrations: %v", err)
	}

	buildInfo := &cmd.BuildInfo{
		App:     appName,
		Version: version,
		Commit:  commit,
		Date:    date,
		BuiltBy: builtBy,
	}

	if err := cmd.Root(buildInfo, migrationsSub).Run(os.Args); err != nil {
		log.Printf("error: %v", err)
		os.Exit(1)
	}
}
